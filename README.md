<p align="center">
	<img  style="width: 100px" alt="logo" src="https://gitee.com/jack0240/one-by-one/raw/master/yiyi.png">
</p>
<h1 align="center" style="margin: 20px 0 30px; font-weight: bold;">Yi Yi v0.0.1</h1>
<h4 align="center">基于 Vue 和 Spring Boot 的后台管理系统</h4>
<p align="center">
	<a href='https://gitee.com/jack0240/one-by-one/stargazers'><img src='https://gitee.com/jack0240/one-by-one/badge/star.svg?theme=dark' alt='star'></img></a>
    <a href='https://gitee.com/jack0240/one-by-one'><img src='https://gitee.com/jack0240/one-by-one/widgets/widget_3.svg' alt='Fork me on Gitee'></img></a>
    <a href='https://gitee.com/jack0240/one-by-one/members'><img src='https://gitee.com/jack0240/one-by-one/badge/fork.svg?theme=dark' alt='fork'></img></a>
</p>

# 一一 权限管理系统
>名称由来：根据电影《一一》（A One and a Two）电影而得名，万物一生二二生三，三生万物。
系统也是由此一点点代码组成的，所以让我们一点点走进编程世界吧。

# 1. 简介
大四开始到现在一直在做项目，有毕业设计、有公司的项目，也做了很多了，
但是基础的权限管理系统虽说之前做过一次，但是使用的技术都比较老旧。
于是这么久了就想重新做个基础的权限框架，拿来即用，在此基础上继续开发。

YiYi-Vue 前端代码地址：
[https://gitee.com/jack0240/yiyi-vue](https://gitee.com/jack0240/yiyi-vue)

# 2. 使用

## 2.1 开发环境
|      软件/组件       |    版本    |               功能                     |
|--------------------|-----------|----------------------------------------|
| JDK                | 17.0.2+   | Java开发环境                             |
| Maven              | 3.8.6+    | 打包编译，包管理                          |
| MySQL              | 8.0.33+   | 数据库存储                               |
| SpringBoot         | 3.1.1     | 框架                                    |
| SpringSecurity     | 6.1.1     | 角色权限控制授权、安全访问                  |
| Lombok             | 1.18.28   | 简洁代码，不要再写getter或equals方法       |
| HuTool             | 5.8.20    | 小而全的 Java 工具                       |
| MyBatis Plus       | 3.5.3.1   | MyBatis 的增强工具                       |
| knife4j            | 4.1.0     | 接口文档说明工具                          |

[Java环境配置/JDK安装配置](https://jackwei.blog.csdn.net/article/details/86550186) \
[Maven的安装+配置本地仓库路径](https://jackwei.blog.csdn.net/article/details/93717865) \
[MySQL8.0和5.7安装教程](https://jackwei.blog.csdn.net/article/details/86908034)

相应的视频教程可以到博主的B站查看：[Jack魏1996](https://space.bilibili.com/476515241/)

## 2.2 测试环境：
Apifox 2.2.15+  
Apifox 接口分享连接： [https://yiyi-api.apifox.cn](https://yiyi-api.apifox.cn/)

# 3. 更新日志

|        版本        |                  特性                       |    时间    |
|-------------------|--------------------------------------------|------------|
| 0.0.1-SNAPSHOT    | [MyBatis Plus自动驼峰命名、角色/用户管理]()     | 2023-07-15 |
| 0.0.1-SNAPSHOT    | [MyBatis Plus自动填充、页面管理]()            | 2023-07-14 |
| 0.0.1-SNAPSHOT    | [统一代码风格]()                             | 2023-07-10 |
| 0.0.1-SNAPSHOT    | [knife4j集成]()                             | 2023-07-07 |
| 0.0.1-SNAPSHOT    | [SpringSecurity基础]()                      | 2023-02-12 |
| 0.0.1-SNAPSHOT    | [MyBatis Plus集成]()                        | 2023-01-11 |
| 0.0.1-SNAPSHOT    | [SpringDoc集成]()                           | 2023-01-09 |
| 0.0.1-SNAPSHOT    | [前端封装返回数据，全局异常处理]()               | 2023-01-01 |
| 0.0.1-SNAPSHOT    | [MVC基本数据库交互]()                         | 2022-11-12 |
| 0.0.1-SNAPSHOT    | [MyBatis集成]()                             | 2022-11-12 |
| 0.0.1-SNAPSHOT    | [Aop日志输出]()                              | 2022-11-11 |
| 0.0.1-SNAPSHOT    | [Swagger3 文档配置添加]()                     | 2022-10-28 |
| 0.0.1-SNAPSHOT    | [controller 接口添加]()                      | 2022-10-27 |
| 0.0.1-SNAPSHOT    | [Log4j2 日志配置]()                          | 2022-10-26 |
| 0.0.1-SNAPSHOT    | [项目搭建]()                                 | 2022-07-27 |

# 4. 开发贡献
如果大家觉得这个项目还不错，有想贡献一下代码的同学，可以先从这里了解一下贡献流程哟~
1. 拉取代码：[Git代码提交规范](https://blog.csdn.net/WeiHao0240/article/details/131277400)
2. 提交规范：[Git团队代码规范](https://blog.csdn.net/WeiHao0240/article/details/131317234)
3. 代码风格：[Java代码风格统一](https://blog.csdn.net/WeiHao0240/article/details/131627911)
4. 版权信息：[IDEA设置版权信息](https://blog.csdn.net/WeiHao0240/article/details/124334202)
