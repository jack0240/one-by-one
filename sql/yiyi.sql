/*
 Copyright (c) Jack魏 2023 - 2023, All Rights Reserved.
 Date: 2023-01-07 21:21:21
 MySQL版本：8.0.31
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- 1.系统用户表
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` varchar(32) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT '32位唯一用户id',
  `account` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '账号（登录唯一用户名）',
  `password` varchar(64) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT '密码',
  `user_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户名称（昵称）',
  `head_image` varchar(500) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT '头像',
  `email` varchar(64) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT '邮箱',
  `phone` varchar(32) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT '手机号',
  `sex` tinyint NULL DEFAULT NULL COMMENT '性别：0女，1男',
  `status` tinyint NULL DEFAULT 0 COMMENT '状态：0正常，1禁用',
  `notes` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `is_del` tinyint NULL DEFAULT 0 COMMENT '是否删除',
  `update_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- 系统用户表模拟数据
-- ----------------------------
INSERT INTO `sys_user` VALUES ('d8946d3cdb374f31936da9d26d67bb80', 'JackWei', '$2a$15$dH85JCoVoRJ2SrZbXb1WwOpEHpshzuksM2QD01nm5ExaTQV/0H5jq', 'Jack魏', 'https://profile-avatar.csdnimg.cn/5715aa658dd04ab3b65e86e236d9503f_weihao0240.jpg!1', 'jackwei1996@qq.com', '177888999666', 0, 0, NULL, 0,'2022-11-29 22:55:00', '2022-11-29 22:55:00');
INSERT INTO `sys_user` VALUES ('fdfed5bab1c44bc39ec42c25b6bf5ddf', 'admin', '$2a$15$dH85JCoVoRJ2SrZbXb1WwOpEHpshzuksM2QD01nm5ExaTQV/0H5jq', 'Admin', 'https://profile-avatar.csdnimg.cn/5715aa658dd04ab3b65e86e236d9503f_weihao0240.jpg!1', 'jackwei1996@qq.com', '177888999666', 1, 0, NULL, 0,'2022-11-29 22:43:08', '2022-11-29 22:43:08');

-- ----------------------------
-- 2.页面表
-- ----------------------------
DROP TABLE IF EXISTS `sys_page`;
CREATE TABLE `sys_page`  (
    `id` int UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '页面id',
    `group` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '分组',
    `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '页面名称',
    `type` tinyint NULL DEFAULT NULL COMMENT '权限类型：目录 1，菜单 2，按钮 3',
    `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'URL路径',
    `component` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '组件',
    `permission` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '权限许可',
    `method` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '访问方法',
    `sort` int NULL DEFAULT NULL COMMENT '排序',
    `parent_id` int NULL DEFAULT NULL COMMENT '父id',
    `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图标',
    `is_del` tinyint NULL DEFAULT 0 COMMENT '是否删除：0正常，1删除',
    `update_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
    `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '页面表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- 页面表模拟数据
-- ----------------------------
INSERT INTO `sys_page` VALUES (1, '系统管理', '系统管理', 1, NULL, 'sys', NULL, 'get', 1, 0, NULL, 0, '2023-07-01 22:09:26', '2023-07-01 22:09:26');
INSERT INTO `sys_page` VALUES (2, '系统管理', '用户管理', 2, 'userManage', 'userManage/userManage.vue', NULL, 'get', 1, 1, NULL, 0, '2023-07-01 22:09:26', '2023-07-01 22:09:26');
INSERT INTO `sys_page` VALUES (3, '系统管理', '页面管理', 2, 'pageManage', 'pageManage/pageManage.vue', NULL, 'get', 2, 1, NULL, 0, '2023-07-01 22:09:26', '2023-07-01 22:09:26');
INSERT INTO `sys_page` VALUES (4, '系统管理', '角色管理', 2, 'roleManage', 'roleManage/roleManage.vue', NULL, 'get', 3, 1, NULL, 0, '2023-07-01 22:09:26', '2023-07-01 22:09:26');

-- ----------------------------
-- 3.角色表
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
    `id` int UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '角色id',
    `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '角色名',
    `key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '权限字符',
    `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '描述',
    `sort` int NULL DEFAULT NULL COMMENT '排序',
    `status` tinyint NULL DEFAULT 0 COMMENT '状态：0正常、1停用、2默认',
    `is_del` tinyint NULL DEFAULT 0 COMMENT '逻辑删除：0未删除、1删除',
    `update_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
    `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- 角色表模拟数据
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '超级管理员', 'superAdmin', '最大超级管理员', 0, 0, 0, NULL, '2023-07-15 23:19:58');
INSERT INTO `sys_role` VALUES (2, '管理员', 'admin', '管理员', 1, 0, 0, NULL, '2023-07-15 23:19:58');
INSERT INTO `sys_role` VALUES (3, '普通用户', 'user', '普通用户', 2, 0, 0, NULL, '2023-07-15 23:19:58');

-- ----------------------------
-- 4.用户角色表
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
    `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
    `user_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户id',
    `role_id` int NULL DEFAULT NULL COMMENT '角色id',
    `is_del` tinyint NULL DEFAULT 0 COMMENT '逻辑删除：0未删除、1删除',
    `update_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
    `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- 用户角色表模拟数据
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 'd8946d3cdb374f31936da9d26d67bb80', 1, 0, NULL, '2023-07-21 21:21:11');
INSERT INTO `sys_user_role` VALUES (2, 'fdfed5bab1c44bc39ec42c25b6bf5ddf', 1, 0, NULL, '2023-08-26 21:40:29');

-- ----------------------------
-- 5.角色页面表
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_page`;
CREATE TABLE `sys_role_page`  (
    `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
    `role_id` int NULL DEFAULT NULL COMMENT '角色id',
    `page_id` int NULL DEFAULT NULL COMMENT '页面id',
    `is_del` tinyint NULL DEFAULT 0 COMMENT '逻辑删除：0未删除、1删除',
    `update_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
    `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色页面管理' ROW_FORMAT = Dynamic;

-- ----------------------------
-- 角色页面表模拟数据
-- ----------------------------
INSERT INTO `sys_role_page` VALUES (1, 1, 1, 0, NULL, '2023-07-21 22:14:11');
INSERT INTO `sys_role_page` VALUES (2, 1, 2, 0, NULL, '2023-07-21 22:14:18');
INSERT INTO `sys_role_page` VALUES (3, 1, 3, 0, NULL, '2023-07-21 22:14:21');
INSERT INTO `sys_role_page` VALUES (4, 1, 4, 0, NULL, '2023-07-21 22:14:24');

SET FOREIGN_KEY_CHECKS = 1;
