/*
 * Copyright (c) Jack魏 2022 - 2022, All Rights Reserved.
 */

package cn.jackwei.yiyi;

import lombok.extern.log4j.Log4j2;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * 启动类
 *
 * @author Jack魏
 * @since 2022-07-27
 */
@Log4j2
@SpringBootApplication
@MapperScan("cn.jackwei.yiyi.mapper")
public class YiYiApplication {
    /**
     * 主方法
     *
     * @param args 运行参数
     */
    public static void main(String[] args) throws UnknownHostException {
        SpringApplication.run(YiYiApplication.class, args);

        String ip = InetAddress.getLocalHost().getHostAddress();
        String port = System.getProperty("server.port");
        String path = System.getProperty("server.servlet.context-path");
        String url = "http://" + ip + ":" + port + path;
        String swaggerPrefix = path.endsWith("/") ? "" : "/";
        log.info("SpringDoc UI：{}doc.html", url + swaggerPrefix);
    }
}
