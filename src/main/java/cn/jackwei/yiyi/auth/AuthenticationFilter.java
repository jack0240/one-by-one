/*
 * Copyright (c) Jack魏 2023 - 2023, All Rights Reserved.
 */

package cn.jackwei.yiyi.auth;

import cn.jackwei.yiyi.pojo.bean.SysUser;
import cn.jackwei.yiyi.pojo.constant.CommonConsts;
import cn.jackwei.yiyi.pojo.enums.ResultMsgEnum;
import cn.jackwei.yiyi.service.sys.SysUserService;
import cn.jackwei.yiyi.util.JwtUtil;

import cn.hutool.core.util.StrUtil;
import jakarta.annotation.Resource;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

/**
 * Security权限过滤器
 *
 * @author Jack魏
 * @since 2023/2/15 0:14
 */
@Component
public class AuthenticationFilter extends OncePerRequestFilter {
    /**
     * 登录/退出接口，需要放行
     */
    private static final String LOGIN_URL = "/sys/login";
    private static final String LOGOUT_URL = "/sys/logout";

    @Resource
    private SysUserService sysUserService;

    /**
     * Security过滤器，每次请求都会经过这里
     *
     * @param request     前端请求
     * @param response    后端响应
     * @param filterChain 过滤链
     * @throws ServletException servlet异常
     * @throws IOException      io异常
     */
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
        FilterChain filterChain) throws ServletException, IOException {
        // 放行登录接口
        if (isPass(request.getRequestURI())) {
            filterChain.doFilter(request, response);
            return;
        }
        // 从请求头里获取token值
        final String token = request.getHeader(CommonConsts.AUTHORIZATION);
        try {
            String tip = JwtUtil.verify(token);
            if (StrUtil.isNotBlank(tip)) {
                throw new BadCredentialsException(ResultMsgEnum.LOGIN_AGAIN.getMsg());
            }
        } catch (Exception exception) {
            throw new BadCredentialsException(ResultMsgEnum.LOGIN_AGAIN.getMsg());
        }

        String userId = (String) JwtUtil.getClaim(token, CommonConsts.USER_ID);
        final SysUser user = sysUserService.getByIdAndPage(userId);
        if (user == null) {
            throw new BadCredentialsException(ResultMsgEnum.LOGIN_AGAIN.getMsg());
        }
        if (user.getStatus() != 0) {
            throw new BadCredentialsException(ResultMsgEnum.LOGIN_STATUS_ERROR.getMsg());
        }
        // 从数据库查询用户信息
        CustomUserDetails userDetails = new CustomUserDetails(user);
        // 将用户详情数据存储到全局上下文里，后面可以直接拿出来用户信息
        SecurityContextHolder.getContext().setAuthentication(UsernamePasswordAuthenticationToken
            .authenticated(userDetails, null, null));
        filterChain.doFilter(request, response);
    }

    /**
     * 是否放行
     *
     * @param url 访问链接
     * @return 是否放行
     */
    private boolean isPass(String url) {
        boolean flag = false;
        flag = flag || url.endsWith(LOGIN_URL);
        flag = flag || url.endsWith(LOGOUT_URL);
        flag = flag || url.contains("swagger")
            || url.contains("/v3/api-docs")
            || url.contains("/doc.html")
            || url.contains("webjars");
        return flag;
    }
}
