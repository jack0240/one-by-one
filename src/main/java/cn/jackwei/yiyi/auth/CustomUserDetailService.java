/*
 * Copyright (c) Jack魏 2023 - 2023, All Rights Reserved.
 */

package cn.jackwei.yiyi.auth;

import cn.jackwei.yiyi.pojo.bean.SysUser;
import cn.jackwei.yiyi.pojo.enums.ResultMsgEnum;
import cn.jackwei.yiyi.service.sys.SysUserService;

import jakarta.annotation.Resource;
import lombok.extern.log4j.Log4j2;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * 用户登录验证
 *
 * @author Jack魏
 * @since 2023-02-08
 */
@Log4j2
@Service
public class CustomUserDetailService implements UserDetailsService {
    @Resource
    private SysUserService sysUserService;

    /**
     * 根据用户账号登录
     *
     * @param account 账号
     * @return 用户信息
     * @throws UsernameNotFoundException 异常
     */
    @Override
    public UserDetails loadUserByUsername(String account) throws UsernameNotFoundException {
        SysUser user = sysUserService.getByAccount(account);
        if (user == null) {
            log.warn(account + "账号不存在！");
            throw new UsernameNotFoundException("账号为：" + account + "不存在，请注册！");
        } else if (user.getStatus() != 0) {
            log.warn(account + ResultMsgEnum.LOGIN_STATUS_ERROR.getMsg());
            throw new LockedException(ResultMsgEnum.LOGIN_STATUS_ERROR.getMsg());
        }
        return new CustomUserDetails(user);
    }
}
