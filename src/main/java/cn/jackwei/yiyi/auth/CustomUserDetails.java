package cn.jackwei.yiyi.auth;

import cn.jackwei.yiyi.pojo.bean.SysUser;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;

/**
 * 适配用户类，这里需要实现security里面的UserDetails接口
 *
 * @author Jack魏
 * @since 2023-02-08
 */
@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomUserDetails extends SysUser implements UserDetails {
    /**
     * 构造用户信息
     *
     * @param user 用户信息
     */
    public CustomUserDetails(SysUser user) {
        super(user.getId(), user.getAccount(), user.getPassword(), user.getUserName(), user.getHeadImage(),
            user.getEmail(), user.getPhone(), user.getSex(), user.getStatus(), user.getNotes(),
            user.getIsDel(), user.getUpdateTime(),
            user.getCreateTime(), user.getPageList(), user.getRoleIds(), user.getRoleNames());
    }

    /**
     * 权限列表
     *
     * @return 权限列表
     */
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.EMPTY_LIST;
    }

    /**
     * 用户名
     *
     * @return 用户名
     */
    @Override
    public String getUsername() {
        return super.getAccount();
    }

    /**
     * 账户是否锁定
     *
     * @return 默认为true
     */
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    /**
     * 账户是否失效
     *
     * @return 默认为true
     */
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    /**
     * 密码是否失效
     *
     * @return 默认为true
     */
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    /**
     * 是否可用
     *
     * @return 默认为true
     */
    @Override
    public boolean isEnabled() {
        return true;
    }
}
