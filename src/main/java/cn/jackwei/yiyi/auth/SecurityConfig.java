/*
 * Copyright (c) Jack魏 2023 - 2023, All Rights Reserved.
 */

package cn.jackwei.yiyi.auth;

import cn.jackwei.yiyi.pojo.enums.ResultMsgEnum;
import cn.jackwei.yiyi.util.Result;

import cn.hutool.json.JSONUtil;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.access.ExceptionTranslationFilter;

/**
 * Security自定义配置类 官网教程: https://docs.spring.io/spring-security/reference/servlet/configuration/java.html
 * 注意：在5.7.0之后废弃了WebSecurityConfigurerAdapter
 *
 * @author Jack魏
 * @since 2023/1/5 22:55
 */
@Log4j2
@Configuration
@EnableWebSecurity
public class SecurityConfig {
    @Autowired
    private CustomUserDetailService userDetailService;

    @Autowired
    private AuthenticationFilter authenticationFilter;

    /**
     * 密码加密方式，如果没有这个会报错：
     *
     * @return 用户加密后密码
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        // 官方推荐使用BCrypt加密方案：随机生成salt混入加密后密码，迭代2^8次
        return new BCryptPasswordEncoder(8);
    }

    /**
     * 过滤链
     *
     * @param httpSecurity 请求
     * @return 请求构建
     * @throws Exception 异常
     */
    @Bean
    public SecurityFilterChain filterChain(HttpSecurity httpSecurity) throws Exception {
        // 关闭CSRF，我们使用前后端项目需要使用RESTful开发，暂时关闭。
        httpSecurity.csrf(csrf -> csrf.disable())
            // 关闭登录框
            .formLogin(Customizer.withDefaults())
            // 添加自定义的过滤器链
            .addFilterAfter(authenticationFilter, ExceptionTranslationFilter.class)
            // 不创建session，不用HttpSession获取SecurityContext
            .sessionManagement(session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
            // 这里的异常不受全异常处理类控制，需要这里输出到页面提示
            .exceptionHandling(hand -> hand.authenticationEntryPoint((request, response, authException) -> {
                response.setContentType("application/json;charset=UTF-8");
                if (authException.getStackTrace()[0].getClassName().endsWith("AuthenticationFilter")) {
                    response.getWriter().write(JSONUtil.toJsonStr(Result.fail(ResultMsgEnum.LOGIN_AGAIN.getCode(),
                        authException.getMessage())));
                } else {
                    response.getWriter().write(JSONUtil.toJsonStr(Result.fail(authException.getMessage())));
                }
            }).accessDeniedHandler((request, response,
                accessDeniedException) -> {
                response.setContentType("application/json;charset=UTF-8");
                response.getWriter().write(JSONUtil.toJsonStr(Result.fail(accessDeniedException.getMessage())));
                if (accessDeniedException.getStackTrace()[0].getClassName().endsWith("AuthenticationFilter")) {
                    response.getWriter().write(JSONUtil.toJsonStr(Result.fail(ResultMsgEnum.LOGIN_AGAIN.getCode(),
                        accessDeniedException.getMessage())));
                } else {
                    response.getWriter().write(JSONUtil.toJsonStr(Result.fail(accessDeniedException.getMessage())));
                }
            }));
        return httpSecurity.build();
    }

    /**
     * 认证管理
     *
     * @return 认证管理配置
     */
    @Bean
    public AuthenticationManager authenticationManager() {
        DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();
        // 调用loadUserByUsername方法获取用户信息
        daoAuthenticationProvider.setUserDetailsService(userDetailService);
        // 通过自定义加密方式去验证密码是否正确
        daoAuthenticationProvider.setPasswordEncoder(passwordEncoder());
        return new ProviderManager(daoAuthenticationProvider);
    }
}
