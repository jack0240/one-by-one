/*
 * Copyright (c) Jack魏 2022 - 2022 , All Rights Reserved.
 */

package cn.jackwei.yiyi.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 应用配置
 *
 * @author Jack魏
 * @since 2022/11/11
 */
@Configuration
public class AppConfig {
    /**
     * 服务器的HTTP端口
     */
    @Value("${server.port}")
    public String port;

    /**
     * 应用的访问路径
     */
    @Value("${server.servlet.context-path}")
    public String path;

    /**
     * 设置系统配置
     */
    @Bean
    public void setSystemProperty() {
        System.setProperty("server.port", port);
        System.setProperty("server.servlet.context-path", path);
    }
}
