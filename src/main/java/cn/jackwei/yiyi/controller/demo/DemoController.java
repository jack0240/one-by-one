/*
 * Copyright (c) Jack魏 2022 - 2022 , All Rights Reserved.
 */

package cn.jackwei.yiyi.controller.demo;

import cn.jackwei.yiyi.util.Result;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 测试controller
 * 注意这里的RestController注解，是返回给前端json格式的。
 * RequestMapping注解就是接口前缀
 *
 * @author Jack魏
 * @since 2022-10-26
 */
@Tag(name = "演示接口", description = "演示测试接口")
@RestController
@RequestMapping("/demo")
public class DemoController {
    /**
     * 日志打印
     */
    private static final Logger logger = LoggerFactory.getLogger(DemoController.class);

    /**
     * hello 接口
     *
     * @return hello字符串
     */
    @Operation(summary = "hello 接口", description = "访问此接口会返回：hello")
    @GetMapping("/hello")
    public String hello() {
        logger.info("hello");
        return "hello";
    }

    /**
     * hello name 接口
     *
     * @return 字符串
     */
    @Operation(summary = "hello name 接口", description = "访问此接口会根据参数返回：hello 参数")
    @Parameter(name = "name", description = "名字")
    @GetMapping("/helloName")
    public String helloName(String name) {
        logger.warn("hello {}", name);
        return "hello " + name;
    }

    /**
     * 测试异常返回
     *
     * @param num 参数：负数报异常，否则正常输出错误信息
     * @return 错误信息
     */
    @Operation(summary = "测试异常返回", description = "根据参数输出异常信息")
    @Parameter(name = "num", description = "负数报异常，否则正常输出错误信息")
    @GetMapping("/exception")
    public Result<String> exception(Integer num) {
        boolean flag = num < 0;
        if (flag) {
            throw new IllegalArgumentException("x must be nonnegative");
        }
        return Result.fail("exception");
    }
}
