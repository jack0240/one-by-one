/*
 * Copyright (c) Jack魏 2023 - 2023, All Rights Reserved.
 */

package cn.jackwei.yiyi.controller.demo;

import cn.jackwei.yiyi.util.Result;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 这里是测试Security 登录后返回的数据
 *
 * @author Jack魏
 * @since 2023/2/8 22:32
 */
@Tag(name = "根路径", description = "根路径接口")
@RestController
@RequestMapping("/")
public class IndexController {
    /**
     * 根目录返回数据
     *
     * @return 根目录数据信息
     */
    @GetMapping()
    @Operation(summary = "访问根目录", description = "这里是根目录信息")
    public Result<String> index() {
        return Result.success("这里是根目录信息");
    }
}
