/*
 * Copyright (c) Jack魏 2023 - 2023, All Rights Reserved.
 */

package cn.jackwei.yiyi.controller.sys;

import cn.jackwei.yiyi.pojo.bean.SysPage;
import cn.jackwei.yiyi.pojo.bean.SysRole;
import cn.jackwei.yiyi.pojo.bean.SysRolePage;
import cn.jackwei.yiyi.pojo.bean.SysUser;
import cn.jackwei.yiyi.pojo.bean.SysUserRole;
import cn.jackwei.yiyi.pojo.enums.ResultMsgEnum;
import cn.jackwei.yiyi.pojo.query.SysRoleQuery;
import cn.jackwei.yiyi.service.sys.SysPageService;
import cn.jackwei.yiyi.service.sys.SysRolePageService;
import cn.jackwei.yiyi.service.sys.SysRoleService;
import cn.jackwei.yiyi.service.sys.SysUserRoleService;
import cn.jackwei.yiyi.util.Result;

import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 功能介绍
 *
 * @author Jack魏
 * @since 2023/7/12 22:30
 */
@Tag(name = "系统页面权限接口", description = "系统页面权限接口")
@RestController
@RequestMapping("/sys/pageRole")
public class SysPageRoleController {
    @Autowired
    private SysPageService sysPageService;

    @Autowired
    private SysRoleService sysRoleService;

    @Autowired
    private SysUserRoleService sysUserRoleService;

    @Autowired
    private SysRolePageService sysRolePageService;

    /**
     * 新增、修改页面
     *
     * @param sysPage 页面信息
     * @return 新增、修改页面
     */
    @Operation(summary = "新增、修改页面", description = "新增、修改页面")
    @PostMapping("/editPage")
    public Result<String> editPage(@RequestBody SysPage sysPage) {
        return sysPageService.editPage(sysPage);
    }

    /**
     * 根据id删除页面
     *
     * @param id 页面id
     * @return 删除是否成功
     */
    @Operation(summary = "根据id删除页面", description = "根据id删除页面")
    @Parameter(name = "id", description = "页面id")
    @PostMapping("/delPageById")
    public Result<String> delPageById(@RequestParam Integer id) {
        final boolean flag = sysPageService.removeById(id);
        if (flag) {
            return Result.successMsg(ResultMsgEnum.SUCCESS_DELETE.getMsg());
        }
        return Result.fail(ResultMsgEnum.ERROR_OPERATE.getMsg());
    }

    /**
     * 查询所有页面列表（不分页树形结构）
     *
     * @return 查询列表
     */
    @Operation(summary = "查询页面列表", description = "查询所有页面列表（不分页树形结构）")
    @GetMapping("/listPageTree")
    public Result<List<SysPage>> listPageTree() {
        return sysPageService.listPageTree();
    }

    /**
     * 新增、修改角色
     *
     * @param sysRole 角色信息
     * @return 新增、修改角色
     */
    @Operation(summary = "新增、修改角色", description = "新增、修改角色")
    @PostMapping("/editRole")
    public Result<String> editRole(@RequestBody SysRole sysRole) {
        return sysRoleService.editRole(sysRole);
    }

    /**
     * 根据id删除角色
     *
     * @param id 角色id
     * @return 删除是否成功
     */
    @Operation(summary = "根据id删除角色", description = "根据id删除角色")
    @Parameter(name = "id", description = "角色id")
    @PostMapping("/delRoleById")
    public Result<String> delRoleById(@RequestParam Integer id) {
        return sysRoleService.delRoleById(id);
    }

    /**
     * 查询所有角色列表（不分页）
     *
     * @return 查询列表
     */
    @Operation(summary = "查询角色列表", description = "查询所有角色列表")
    @GetMapping("/listRole")
    public Result<List<SysRole>> listRole() {
        return Result.success(sysRoleService.list());
    }

    /**
     * 查询角色列表（分页）
     *
     * @param query 查询条件
     * @return 查询列表
     */
    @Operation(summary = "查询角色列表（分页）", description = "分页查询角色列表")
    @GetMapping("/pageRole")
    public Result<Page> pageRole(SysRoleQuery query) {
        return Result.success(sysRoleService.pageRole(query));
    }

    /**
     * 用户分配角色（一个用户一个角色）
     *
     * @param sysUserRole 用户角色信息
     * @return 新增、修改用户角色信息
     */
    @Operation(summary = "用户分配角色", description = "给用户分配角色（一个用户一个角色）")
    @PostMapping("/editUserRole")
    public Result<String> editUserRole(@RequestBody SysUserRole sysUserRole) {
        return sysUserRoleService.editUserRole(sysUserRole);
    }

    /**
     * 分配角色页面（一个角色多个页面）
     *
     * @param sysUserRole 用户角色信息
     * @return 新增、修改用户角色信息
     */
    @Operation(summary = "分配角色页面", description = "分配角色页面（一个角色多个页面）")
    @PostMapping("/editRolePage")
    public Result<String> editRolePage(@RequestBody SysRolePage sysUserRole) {
        return sysRolePageService.editRolePage(sysUserRole);
    }

    /**
     * 批量取消用户角色
     *
     * @param jsonObject 用户id，用逗号隔开。
     * @return 是否成功
     */
    @Operation(summary = "批量取消用户角色", description = "批量取消用户角色（用户id，用逗号隔开）")
    @PostMapping("/delUserRoleBatch")
    public Result<String> delUserRoleBatch(@RequestBody JSONObject jsonObject) {
        final String userIds = jsonObject.get("userIds").toString();
        return sysUserRoleService.delUserRoleBatch(userIds);
    }

    /**
     * 根据用户id获取角色
     *
     * @param userId 用户id
     * @return 全部用户角色
     */
    @Operation(summary = "根据用户id获取其角色", description = "全部用户的角色")
    @GetMapping("/listRoleByUserId")
    public Result<List<SysRole>> listRoleByUserId(@RequestParam String userId) {
        final List<SysRole> roleList = sysRoleService.listRoleByUserId(userId);
        return Result.success(roleList);
    }

    /**
     * 根据角色id获取用户
     *
     * @param roleId 角色id
     * @return 全部角色用户
     */
    @Operation(summary = "根据角色id获取其下用户", description = "全部角色的用户")
    @GetMapping("/listUserByRoleId")
    public Result<List<SysUser>> listUserByRoleId(@RequestParam String roleId) {
        final List<SysUser> list = sysRoleService.listUserByRoleId(roleId);
        return Result.success(list);
    }

    /**
     * 根据角色id获取页面
     *
     * @param roleId 角色id
     * @return 全部角色页面
     */
    @Operation(summary = "根据角色id获取其下页面", description = "全部角色的页面")
    @GetMapping("/listPageByRoleId")
    public Result<List<SysPage>> listPageByRoleId(@RequestParam String roleId) {
        final List<SysPage> list = sysPageService.listPageByRoleId(roleId);
        return Result.success(list);
    }
}
