/*
 * Copyright (c) Jack魏 2022 - 2023, All Rights Reserved.
 */

package cn.jackwei.yiyi.controller.sys;

import cn.jackwei.yiyi.pojo.bean.SysUser;
import cn.jackwei.yiyi.pojo.enums.ResultMsgEnum;
import cn.jackwei.yiyi.pojo.query.SysUserQuery;
import cn.jackwei.yiyi.service.sys.SysUserService;
import cn.jackwei.yiyi.util.Result;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 系统用户Controller控制类
 *
 * @author Jack魏
 * @since 2022-11-12
 */
@Tag(name = "系统用户接口", description = "系统用户接口")
@RestController
@RequestMapping("/sys/user")
public class SysUserController {
    @Autowired
    private SysUserService sysUserService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    /**
     * 注册后台系统用户
     *
     * @param sysUser 前端注册信息
     * @return 注册用户信息
     */
    @Operation(summary = "注册接口", description = "输入用户信息进行注册")
    @PostMapping("/register")
    public Result<String> register(@RequestBody SysUser sysUser) {
        // 登录名参数校验
        if (sysUser.getAccount() == null || "".equals(sysUser.getAccount())) {
            return Result.fail("注册用户名不能为空");
        }
        // 密码参数校验
        if (sysUser.getPassword() == null || "".equals(sysUser.getPassword())) {
            return Result.fail("密码不能为空");
        }
        sysUser.setPassword(passwordEncoder.encode(sysUser.getPassword()));
        SysUser user = sysUserService.register(sysUser);
        if (user == null) {
            return Result.fail("注册失败！请更换登录名！");
        }
        return Result.successMsg("注册成功，欢迎您：" + sysUser.getAccount());
    }

    /**
     * 删除用户
     *
     * @param userId 用户id
     * @return 删除是否成功
     */
    @Operation(summary = "删除用户", description = "根据用户id删除用户")
    @PostMapping("/delUser")
    public Result<String> delUser(@RequestParam String userId) {
        final boolean flag = sysUserService.removeById(userId);
        if (flag) {
            return Result.successMsg(ResultMsgEnum.SUCCESS_DELETE.getMsg());
        }
        return Result.fail(ResultMsgEnum.ERROR_OPERATE.getMsg());
    }

    /**
     * 新增、修改用户
     *
     * @param sysUser 用户信息
     * @return 新增、修改用户
     */
    @Operation(summary = "新增、修改用户", description = "新增、修改用户")
    @PostMapping("/editUser")
    public Result<String> editPage(@RequestBody SysUser sysUser) {
        sysUser.setPassword(passwordEncoder.encode(sysUser.getPassword()));
        return sysUserService.editUser(sysUser);
    }

    /**
     * 返回用户详情
     *
     * @return 用户详细信息
     */
    @Operation(summary = "登录用户信息", description = "返回用户信息")
    @GetMapping("/info")
    public Result<SysUser> info() {
        // 这里是从全局解析里面拿的数据
        SysUser user = (SysUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return Result.success(user);
    }

    /**
     * 用户列表
     *
     * @return 全部用户信息
     */
    @Operation(summary = "用户列表", description = "全部用户信息")
    @GetMapping("/listUser")
    public Result<List<SysUser>> listUser() {
        final List<SysUser> userList = sysUserService.list();
        return Result.success(userList);
    }

    /**
     * 用户列表（分页）
     *
     * @param query 查询条件
     * @return 分页后用户信息和角色id
     */
    @Operation(summary = "用户列表（分页）", description = "分页后用户信息和角色id")
    @GetMapping("/pageUser")
    public Result<Page> pageUser(SysUserQuery query) {
        Page<SysUser> userPage = sysUserService.pageUser(query);
        return Result.success(userPage);
    }
}
