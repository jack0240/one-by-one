/*
 * Copyright (c) Jack魏 2023 - 2023, All Rights Reserved.
 */

package cn.jackwei.yiyi.exception;

/**
 * 业务异常类：业务上的异常由此抛出
 *
 * @author Jack魏
 * @since 2023/2/15 0:10
 */
public class BusinessException extends Exception {
    /**
     * 将异常信息传给父类
     *
     * @param message 异常信息
     */
    public BusinessException(String message) {
        super(message);
    }
}
