/*
 * Copyright (c) Jack魏 2022 - 2022 , All Rights Reserved.
 */

package cn.jackwei.yiyi.exception;

import cn.jackwei.yiyi.util.Result;

import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 全局异常处理类
 *
 * @author Jack魏
 * @since 2022-12-31
 */
@Log4j2
@RestControllerAdvice
public class GlobalExceptionHandler {
    /**
     * 这里处理Exception异常的信息，ResponseStatus这个就是返回前端的状态码（去掉响应是200，否则是500）
     *
     * @param request   请求
     * @param exception 异常
     * @return 返回错误信息
     */
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.OK)
    public Result<String> handleException(HttpServletRequest request, Exception exception) {
        // 如果没有这个，是不会打印错误日志信息的
        log.error(exception.getMessage(), exception);
        return Result.fail(exception.getMessage());
    }
}
