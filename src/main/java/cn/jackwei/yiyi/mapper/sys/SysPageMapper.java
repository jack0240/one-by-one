/*
 * Copyright (c) Jack魏 2023 - 2023, All Rights Reserved.
 */

package cn.jackwei.yiyi.mapper.sys;

import cn.jackwei.yiyi.pojo.bean.SysPage;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 页面管理 Mapper 接口
 * </p>
 *
 * @author Jack魏
 * @since 2023-01-13
 */
@Mapper
public interface SysPageMapper extends BaseMapper<SysPage> {
    /**
     * 根据用户id查询所有用户页面信息
     *
     * @param userId 用户id
     * @return 所有页面信息
     */
    List<SysPage> listUserPage(@Param("userId") String userId);

    /**
     * 根据角色id获取页面
     *
     * @param roleId 角色id
     * @return 全部角色页面
     */
    List<SysPage> listPageByRoleId(@Param("roleId") String roleId);
}
