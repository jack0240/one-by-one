/*
 * Copyright (c) Jack魏 2023 - 2023, All Rights Reserved.
 */

package cn.jackwei.yiyi.mapper.sys;

import cn.jackwei.yiyi.pojo.bean.SysRole;
import cn.jackwei.yiyi.pojo.bean.SysUser;
import cn.jackwei.yiyi.pojo.query.SysRoleQuery;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 角色 Mapper 接口
 * </p>
 *
 * @author Jack魏
 * @since 2023-01-13
 */
@Mapper
public interface SysRoleMapper extends BaseMapper<SysRole> {
    /**
     * 根据用户id获取角色
     *
     * @param userId 用户id
     * @return 全部用户角色
     */
    List<SysRole> listRoleByUserId(@Param("userId") String userId);

    /**
     * 根据角色id获取用户
     *
     * @param roleId 角色id
     * @return 全部角色用户
     */
    List<SysUser> listUserByRoleId(@Param("roleId") String roleId);

    /**
     * 查询角色列表（分页）
     *
     * @param query 查询条件
     * @return 查询列表
     */
    Page<SysRole> pageRoleAndPageId(SysRoleQuery query);
}
