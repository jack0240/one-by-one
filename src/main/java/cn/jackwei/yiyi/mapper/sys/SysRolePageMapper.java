/*
 * Copyright (c) Jack魏 2023 - 2023, All Rights Reserved.
 */

package cn.jackwei.yiyi.mapper.sys;

import cn.jackwei.yiyi.pojo.bean.SysRolePage;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 角色页面管理 Mapper 接口
 * </p>
 *
 * @author Jack魏
 * @since 2023-07-21
 */
@Mapper
public interface SysRolePageMapper extends BaseMapper<SysRolePage> {
    /**
     * 批量删除角色页面
     *
     * @param roleId 角色id
     */
    void deleteBatchRoleId(@Param("roleId") Integer roleId);
}
