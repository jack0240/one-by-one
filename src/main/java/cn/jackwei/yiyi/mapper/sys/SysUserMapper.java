/*
 * Copyright (c) Jack魏 2022 - 2022, All Rights Reserved.
 */

package cn.jackwei.yiyi.mapper.sys;

import cn.jackwei.yiyi.pojo.bean.SysUser;
import cn.jackwei.yiyi.pojo.query.SysUserQuery;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 系统用户管理Mapper
 *
 * @author Jack魏
 * @since 2022-11-11
 */
@Mapper
public interface SysUserMapper extends BaseMapper<SysUser> {
    /**
     * 根据用户名查找是否存在
     *
     * @param account 登录名
     * @return 用户信息
     */
    SysUser selectByAccount(@Param("account") String account);

    /**
     * 用户列表（分页）
     *
     * @param query 查询条件
     * @return 分页后用户信息和角色id
     */
    Page<SysUser> pageUserAndRoleId(SysUserQuery query);
}
