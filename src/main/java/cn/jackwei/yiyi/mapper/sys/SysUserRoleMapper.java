/*
 * Copyright (c) Jack魏 2023 - 2023, All Rights Reserved.
 */

package cn.jackwei.yiyi.mapper.sys;

import cn.jackwei.yiyi.pojo.bean.SysUserRole;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 用户角色表 Mapper 接口
 * </p>
 *
 * @author Jack魏
 * @since 2023-07-21
 */
@Mapper
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {
    /**
     * 批量取消用户角色
     *
     * @param userIds 用户id，用逗号隔开。
     * @return 是否成功
     */
    int delUserRoleBatch(@Param("userIds") String userIds);
}
