/*
 * Copyright (c) Jack魏 2023 - 2023 , All Rights Reserved.
 */

package cn.jackwei.yiyi.pojo.bean;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 页面管理
 * </p>
 *
 * @author Jack魏
 * @since 2023-01-13
 */
@Data
@TableName("sys_page")
@Schema(description = "页面管理")
public class SysPage implements Serializable {
    /**
     * 页面id
     */
    @TableId(value = "id", type = IdType.AUTO)
    @Schema(description = "页面id（为空新增）", defaultValue = "null")
    private Integer id;

    /**
     * 分组
     */
    @TableField("`group`")
    @Schema(description = "分组")
    private String group;

    /**
     * 名称
     */
    @NotNull
    @TableField("`name`")
    @Schema(description = "名称")
    private String name;

    /**
     * 权限类型：目录 1，菜单 2，按钮 3
     */
    @Schema(description = "权限类型：目录 1，菜单 2，按钮 3")
    private Integer type;

    /**
     * URL路径
     */
    @NotNull
    @Schema(description = "URL路径")
    private String url;

    /**
     * 组件
     */
    @NotNull
    @Schema(description = "组件")
    private String component;

    /**
     * 权限许可
     */
    @Schema(description = "权限许可")
    private String permission;

    /**
     * 访问方法
     */
    @Schema(description = "访问方法")
    private String method;

    /**
     * 排序
     */
    @NotNull
    @Schema(description = "排序")
    private Integer sort;

    /**
     * 父id（根目录为0）
     */
    @NotNull
    @Schema(description = "父id（根目录为0）", defaultValue = "0")
    private Integer parentId;

    /**
     * 图标
     */
    @Schema(description = "图标")
    private String icon;

    /**
     * 是否删除：0正常，1删除
     */
    @Schema(description = "是否删除：0正常，1删除")
    private Integer isDel;

    /**
     * 修改时间
     */
    @Schema(description = "修改时间")
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateTime;

    /**
     * 创建时间
     */
    @Schema(description = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 所有子页面
     */
    @TableField(exist = false)
    @Schema(description = "子页面")
    private transient List<SysPage> children;
}
