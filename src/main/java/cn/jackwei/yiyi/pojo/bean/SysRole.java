package cn.jackwei.yiyi.pojo.bean;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 角色
 * </p>
 *
 * @author Jack魏
 * @since 2023-01-13
 */
@Data
@TableName("sys_role")
@Schema(name = "角色类")
public class SysRole implements Serializable {
    /**
     * 角色id
     */
    @TableId(value = "id", type = IdType.AUTO)
    @Schema(description = "角色id")
    private Integer id;

    /**
     * 角色名
     */
    @NotNull
    @Schema(description = "角色名")
    private String name;

    /**
     * 权限字符
     */
    @NotNull
    @TableField("`key`")
    @Schema(description = "权限字符")
    private String key;

    /**
     * 描述
     */
    @Schema(description = "描述")
    private String description;

    /**
     * 排序
     */
    @Schema(description = "排序")
    private Integer sort;

    /**
     * 状态：0正常、1停用、2默认
     */
    @Schema(description = "0正常、1停用、2默认")
    private Integer status;

    /**
     * 逻辑删除：0未删除、1删除
     */
    @Schema(description = "逻辑删除：0未删除、1删除")
    private Integer isDel;

    /**
     * 修改时间
     */
    @Schema(description = "修改时间")
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateTime;

    /**
     * 创建时间
     */
    @Schema(description = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 页面ids
     */
    @TableField(exist = false)
    @Schema(description = "页面ids，逗号隔开")
    private transient String pageIds;
}
