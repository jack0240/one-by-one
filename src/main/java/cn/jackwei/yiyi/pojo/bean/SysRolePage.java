/*
 * Copyright (c) Jack魏 2023 - 2023, All Rights Reserved.
 */

package cn.jackwei.yiyi.pojo.bean;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 角色页面管理
 * </p>
 *
 * @author Jack魏
 * @since 2023-07-21
 */
@Data
@Schema(name = "角色页面表")
@TableName("sys_role_page")
public class SysRolePage implements Serializable {
    @TableId(value = "id", type = IdType.AUTO)
    @Schema(description = "id为空或0新增，有id时直接删除")
    private Integer id;

    /**
     * 角色id
     */
    @Schema(description = "角色id")
    private Integer roleId;

    /**
     * 页面id
     */
    @Schema(description = "页面id")
    private Integer pageId;

    /**
     * 逻辑删除：0未删除、1删除
     */
    @Schema(description = "逻辑删除：0未删除、1删除")
    private Integer isDel;

    /**
     * 修改时间
     */
    @Schema(description = "修改时间")
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateTime;

    /**
     * 创建时间
     */
    @Schema(description = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;
}
