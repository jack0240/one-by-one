/*
 * Copyright (c) Jack魏 2022 - 2023, All Rights Reserved.
 */

package cn.jackwei.yiyi.pojo.bean;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 系统用户类
 *
 * @author Jack魏
 * @since 2022-11-11
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Schema(name = "系统用户类")
public class SysUser implements Serializable {
    /**
     * 32位唯一用户id
     */
    @Schema(description = "32位唯一用户id")
    private String id;

    /**
     * 登录名
     */
    @NotNull
    @Schema(description = "登录名")
    private String account;

    /**
     * 密码
     */
    @NotNull
    @Schema(description = "密码")
    private String password;

    /**
     * 用户名称（昵称）
     */
    @NotNull
    @Schema(description = "用户名称（昵称）")
    private String userName;

    /**
     * 头像
     */
    @Schema(description = "头像")
    private String headImage;

    /**
     * 邮箱
     */
    @Schema(description = "邮箱")
    private String email;

    /**
     * 手机号
     */
    @Schema(description = "手机号")
    private String phone;

    /**
     * 性别：0女，1男
     */
    @Schema(description = "性别：0女，1男")
    private Integer sex;

    /**
     * 状态：0正常，1禁用
     */
    @Schema(description = "状态：0正常，1禁用")
    private Integer status;

    /**
     * 备注
     */
    @Schema(description = "备注")
    private String notes;

    /**
     * 是否删除：0正常，1删除
     */
    @Schema(description = "是否删除：0正常，1删除")
    private Integer isDel;

    /**
     * 更新时间
     */
    @Schema(description = "更新时间")
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateTime;

    /**
     * 创建时间
     */
    @Schema(description = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 所有页面
     */
    @TableField(exist = false)
    @Schema(description = "所有当前用户角色页面")
    private transient List<SysPage> pageList;

    /**
     * 所有角色id
     */
    @TableField(exist = false)
    @Schema(description = "所有角色id")
    private transient String roleIds;

    /**
     * 所有角色名称
     */
    @TableField(exist = false)
    @Schema(description = "所有角色名称")
    private transient String roleNames;
}
