/*
 * Copyright (c) Jack魏 2023 - 2023, All Rights Reserved.
 */

package cn.jackwei.yiyi.pojo.constant;

/**
 * 通用常量类
 *
 * @author Jack魏
 * @since 2023/7/25 22:27
 */
public class CommonConsts {
    /**
     * 请求头Token存储key
     */
    public static final String AUTHORIZATION = "Authorization";

    /**
     * Token前缀
     */
    public static final String TOKEN_PREFIX = "Bearer ";

    /**
     * 用户id
     */
    public static final String USER_ID = "userId";

    /**
     * 用户名
     */
    public static final String USER_NAME = "userName";

    /**
     * 正常标志
     */
    public static final Integer NORMAL = 0;

    /**
     * 删除标志
     */
    public static final Integer DEL = 1;
}
