/*
 * Copyright (c) Jack魏 2023 - 2023, All Rights Reserved.
 */

package cn.jackwei.yiyi.pojo.constant;

/**
 * 符号常量类
 *
 * @author Jack魏
 * @since 2023/7/28 19:40
 */
public class SymbolConsts {
    /**
     * 英文逗号
     */
    public static final String COMMA = ",";
}
