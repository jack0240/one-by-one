/*
 * Copyright (c) Jack魏 2023 - 2023, All Rights Reserved.
 */

package cn.jackwei.yiyi.pojo.enums;

import java.io.Serializable;

/**
 * 基础枚举接口
 *
 * @author Jack魏
 * @since 2023/7/25 23:13
 */
public interface BaseEnum extends Serializable {
    /**
     * 状态码
     *
     * @return 状态码
     */
    int getCode();

    /**
     * 具体信息
     *
     * @return 编码信息
     */
    String getMsg();
}
