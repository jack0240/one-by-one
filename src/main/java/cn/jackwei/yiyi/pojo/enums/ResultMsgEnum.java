/*
 * Copyright (c) Jack魏 2023 - 2023, All Rights Reserved.
 */

package cn.jackwei.yiyi.pojo.enums;

/**
 * 返回消息枚举类
 *
 * @author Jack魏
 * @since 2023/7/25 23:13
 */
public enum ResultMsgEnum implements BaseEnum {
    /**
     * 处理成功
     */
    SUCCESS(200, "处理成功"),
    /**
     * 保存成功
     */
    SUCCESS_SAVE(200, "保存成功"),
    /**
     * 删除成功
     */
    SUCCESS_DELETE(200, "删除成功"),
    /**
     * 修改成功
     */
    SUCCESS_UPDATE(200, "修改成功"),
    /**
     * 请重新登录
     */
    LOGIN_AGAIN(401, "请重新登录"),
    /**
     * 当前用户状态异常
     */
    LOGIN_STATUS_ERROR(403, "当前用户状态异常"),
    /**
     * 已存在
     */
    ERROR_EXISTS(500, "已存在"),
    /**
     * 服务器内部错误
     */
    ERROR_OPERATE(500, "操作失败"),
    /**
     * 服务器内部错误
     */
    ERROR(500, "服务器内部错误");
    /**
     * 编码
     */
    private final int code;
    /**
     * 信息
     */
    private final String msg;

    /**
     * 构造器
     *
     * @param code 状态码
     * @param msg  消息
     */
    ResultMsgEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    @Override
    public int getCode() {
        return this.code;
    }

    @Override
    public String getMsg() {
        return this.msg;
    }
}
