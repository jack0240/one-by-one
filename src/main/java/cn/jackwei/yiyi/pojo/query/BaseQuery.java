/*
 * Copyright (c) Jack魏 2023 - 2023, All Rights Reserved.
 */

package cn.jackwei.yiyi.pojo.query;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 基础查询类
 *
 * @author Jack魏
 * @since 2023/7/28 20:59
 */
@Data
@Schema(description = "基础查询类")
public class BaseQuery extends Page {
    /**
     * 关键字
     */
    @Schema(description = "关键字")
    private String keyWord;

    /**
     * 开始时间
     */
    @Schema(description = "开始时间")
    private String startTime;

    /**
     * 结束时间
     */
    @Schema(description = "结束时间")
    private String endTime;
}
