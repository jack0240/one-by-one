/*
 * Copyright (c) Jack魏 2023 - 2023, All Rights Reserved.
 */

package cn.jackwei.yiyi.pojo.query;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 角色查询类
 *
 * @author Jack魏
 * @since 2023/7/28 22:03
 */
@Data
public class SysRoleQuery extends BaseQuery {
    /**
     * 角色名
     */
    @Schema(description = "角色名")
    private String name;

    /**
     * 权限字符
     */
    @TableField("`key`")
    @Schema(description = "权限字符")
    private String key;

    /**
     * 状态：0正常、1停用、2默认
     */
    @Schema(description = "0正常、1停用、2默认")
    private Integer status;
}
