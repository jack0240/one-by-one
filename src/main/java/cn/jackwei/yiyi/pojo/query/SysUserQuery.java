/*
 * Copyright (c) Jack魏 2023 - 2023, All Rights Reserved.
 */

package cn.jackwei.yiyi.pojo.query;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 系统用户查询类
 *
 * @author Jack魏
 * @since 2023/7/28 21:06
 */
@Data
@Schema(description = "前端返回包装类")
public class SysUserQuery extends BaseQuery {
    /**
     * 状态：0正常，1禁用
     */
    @Schema(description = "状态：0正常，1禁用")
    private Integer status;

    /**
     * 手机号
     */
    @Schema(description = "手机号")
    private String phone;

    /**
     * 登录名
     */
    @Schema(description = "登录名")
    private String account;

    /**
     * 用户名称（昵称）
     */
    @Schema(description = "用户名称（昵称）")
    private String userName;

    /**
     * 角色id
     */
    @Schema(description = "角色id，该用户是否有此角色")
    private Integer roleId;
}
