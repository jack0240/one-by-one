/*
 * Copyright (c) Jack魏 2023 - 2023, All Rights Reserved.
 */

package cn.jackwei.yiyi.service.sys;

import cn.jackwei.yiyi.mapper.sys.SysPageMapper;
import cn.jackwei.yiyi.pojo.bean.SysPage;
import cn.jackwei.yiyi.pojo.enums.ResultMsgEnum;
import cn.jackwei.yiyi.util.Result;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 页面权限管理 服务实现类
 * </p>
 *
 * @author Jack魏
 * @since 2023-01-13
 */
@Service
public class SysPageService extends ServiceImpl<SysPageMapper, SysPage> {
    /**
     * 新增、修改页面
     *
     * @param sysPage 页面信息
     * @return 新增、修改页面
     */
    public Result<String> editPage(SysPage sysPage) {
        if (sysPage.getId() == null || 0 >= sysPage.getId()) {
            final boolean save = save(sysPage);
            if (save) {
                return Result.successMsg(ResultMsgEnum.SUCCESS_SAVE.getMsg());
            }
        } else {
            final boolean update = updateById(sysPage);
            if (update) {
                return Result.successMsg(ResultMsgEnum.SUCCESS_UPDATE.getMsg());
            }
        }
        return Result.fail(ResultMsgEnum.ERROR_OPERATE.getMsg());
    }

    /**
     * 查询所有页面列表（不分页树形结构）
     *
     * @return 查询列表
     */
    public Result<List<SysPage>> listPageTree() {
        LambdaQueryWrapper<SysPage> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(SysPage::getParentId, 0)
            .or()
            .isNull(SysPage::getParentId)
            .orderByAsc(SysPage::getSort);
        List<SysPage> pageList = baseMapper.selectList(wrapper);
        if (pageList != null && pageList.size() > 0) {
            pageList.forEach(this::listAllChild);
        }
        return Result.success(pageList);
    }

    /**
     * 递归查询所有子页面
     *
     * @param sysPage 父页面信息
     */
    private void listAllChild(SysPage sysPage) {
        LambdaQueryWrapper<SysPage> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(SysPage::getParentId, sysPage.getId())
            .orderByAsc(SysPage::getSort);
        List<SysPage> pageList = baseMapper.selectList(wrapper);
        sysPage.setChildren(pageList);
        if (pageList != null && pageList.size() > 0) {
            pageList.forEach(this::listAllChild);
        }
    }

    /**
     * 用户权限页面列表（树形结构）
     *
     * @param userId 用户id
     * @return 权限页面列表
     */
    public List<SysPage> listUserPageTree(String userId) {
        return buildPageTree(baseMapper.listUserPage(userId), 0);
    }

    /**
     * 将查询出来的页面数据构建成树形结构
     *
     * @param pageList 所有页面
     * @param parentId 父id
     * @return
     */
    private List<SysPage> buildPageTree(List<SysPage> pageList, Integer parentId) {
        return pageList.stream()
            .filter(item -> item.getParentId().equals(parentId))
            .map(item -> {
                item.setChildren(buildChildren(item, pageList));
                return item;
            })
            .collect(Collectors.toList());
    }

    /**
     * 递归操作
     *
     * @param page     当前页面信息
     * @param pageList 所有页面信息
     * @return 子页面列表
     */
    private List<SysPage> buildChildren(SysPage page, List<SysPage> pageList) {
        return pageList.stream()
            .filter(item -> item.getParentId().equals(page.getId()))
            .map(item -> {
                item.setChildren(buildChildren(item, pageList));
                return item;
            })
            .collect(Collectors.toList());
    }

    /**
     * 根据角色id获取页面
     *
     * @param roleId 角色id
     * @return 全部角色页面
     */
    public List<SysPage> listPageByRoleId(String roleId) {
        return buildPageTree(baseMapper.listPageByRoleId(roleId), 0);
    }
}
