/*
 * Copyright (c) Jack魏 2023 - 2023, All Rights Reserved.
 */

package cn.jackwei.yiyi.service.sys;

import cn.jackwei.yiyi.mapper.sys.SysRolePageMapper;
import cn.jackwei.yiyi.pojo.bean.SysRolePage;
import cn.jackwei.yiyi.pojo.enums.ResultMsgEnum;
import cn.jackwei.yiyi.util.Result;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 角色权限管理 服务实现类
 * </p>
 *
 * @author Jack魏
 * @since 2023-07-21
 */
@Service
public class SysRolePageService extends ServiceImpl<SysRolePageMapper, SysRolePage> {
    /**
     * 分配角色页面（一个角色多个页面）
     *
     * @param sysUserRole 用户角色信息
     * @return 新增、修改用户角色信息
     */
    public Result<String> editRolePage(SysRolePage sysUserRole) {
        LambdaQueryWrapper<SysRolePage> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(SysRolePage::getRoleId, sysUserRole.getRoleId())
            .eq(SysRolePage::getPageId, sysUserRole.getPageId());
        final List<SysRolePage> sysRolePageList = baseMapper.selectList(wrapper);
        final boolean alive = sysRolePageList != null && !sysRolePageList.isEmpty();

        if (sysUserRole.getId() == null || sysUserRole.getId() == 0) {
            if (!alive) {
                final int flag = baseMapper.insert(sysUserRole);
                if (flag > 0) {
                    return Result.successMsg(ResultMsgEnum.SUCCESS_SAVE.getMsg());
                }
            }
        } else {
            // 有的话就是取消
            final int flag = baseMapper.deleteById(sysUserRole.getId());
            if (flag > 0) {
                return Result.successMsg(ResultMsgEnum.SUCCESS_DELETE.getMsg());
            }
        }
        return Result.fail(ResultMsgEnum.ERROR_OPERATE.getMsg());
    }
}
