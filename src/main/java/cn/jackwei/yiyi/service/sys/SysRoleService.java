/*
 * Copyright (c) Jack魏 2023 - 2023, All Rights Reserved.
 */

package cn.jackwei.yiyi.service.sys;

import cn.jackwei.yiyi.mapper.sys.SysRoleMapper;
import cn.jackwei.yiyi.mapper.sys.SysRolePageMapper;
import cn.jackwei.yiyi.mapper.sys.SysUserRoleMapper;
import cn.jackwei.yiyi.pojo.bean.SysRole;
import cn.jackwei.yiyi.pojo.bean.SysRolePage;
import cn.jackwei.yiyi.pojo.bean.SysUser;
import cn.jackwei.yiyi.pojo.bean.SysUserRole;
import cn.jackwei.yiyi.pojo.constant.CommonConsts;
import cn.jackwei.yiyi.pojo.constant.SymbolConsts;
import cn.jackwei.yiyi.pojo.enums.ResultMsgEnum;
import cn.jackwei.yiyi.pojo.query.SysRoleQuery;
import cn.jackwei.yiyi.util.Result;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 角色 服务实现类
 * </p>
 *
 * @author Jack魏
 * @since 2023-01-13
 */
@Service
public class SysRoleService extends ServiceImpl<SysRoleMapper, SysRole> {
    @Autowired
    private SysRolePageMapper sysRolePageMapper;

    @Autowired
    private SysUserRoleMapper sysUserRoleMapper;

    /**
     * 新增、修改角色
     *
     * @param sysRole 角色信息
     * @return 新增、修改角色
     */
    public Result<String> editRole(SysRole sysRole) {
        if (sysRole.getId() == null || 0 >= sysRole.getId()) {
            final boolean save = save(sysRole);
            if (save) {
                if (StrUtil.isNotBlank(sysRole.getPageIds())) {
                    for (String pageId : sysRole.getPageIds().split(SymbolConsts.COMMA)) {
                        if (StrUtil.isBlank(pageId)) {
                            continue;
                        }
                        SysRolePage sysRolePage = new SysRolePage();
                        sysRolePage.setRoleId(sysRole.getId());
                        sysRolePage.setPageId(Integer.valueOf(pageId));
                        sysRolePageMapper.insert(sysRolePage);
                    }
                }
                return Result.successMsg(ResultMsgEnum.SUCCESS_SAVE.getMsg());
            }
        } else {
            final boolean update = updateById(sysRole);
            if (update) {
                final String[] split = sysRole.getPageIds().split(SymbolConsts.COMMA);
                sysRolePageMapper.deleteBatchRoleId(sysRole.getId());
                for (String pageId : split) {
                    if (StrUtil.isBlank(pageId)) {
                        continue;
                    }
                    SysRolePage sysRolePage = new SysRolePage();
                    sysRolePage.setRoleId(sysRole.getId());
                    sysRolePage.setPageId(Integer.valueOf(pageId));
                    sysRolePageMapper.insert(sysRolePage);
                }
                return Result.successMsg(ResultMsgEnum.SUCCESS_UPDATE.getMsg());
            }
        }
        return Result.fail(ResultMsgEnum.ERROR_OPERATE.getMsg());
    }

    /**
     * 根据用户id获取角色
     *
     * @param userId 用户id
     * @return 全部用户角色
     */
    public List<SysRole> listRoleByUserId(String userId) {
        return baseMapper.listRoleByUserId(userId);
    }

    /**
     * 根据角色id获取用户
     *
     * @param roleId 角色id
     * @return 全部角色用户
     */
    public List<SysUser> listUserByRoleId(String roleId) {
        return baseMapper.listUserByRoleId(roleId);
    }

    /**
     * 查询角色列表（分页）
     *
     * @param query 查询条件
     * @return 查询列表
     */
    public Page<SysRole> pageRole(SysRoleQuery query) {
        return baseMapper.pageRoleAndPageId(query);
    }

    /**
     * 根据id删除角色
     *
     * @param id 角色id
     * @return 删除是否成功
     */
    public Result<String> delRoleById(Integer id) {
        final boolean flag = removeById(id);
        if (flag) {
            // 删除成功后，需要将该角色下的所有用户
            LambdaQueryWrapper<SysUserRole> lambdaQueryWrapper = new LambdaQueryWrapper<>();
            lambdaQueryWrapper.eq(SysUserRole::getRoleId, id);
            SysUserRole sysUserRole = new SysUserRole();
            sysUserRole.setIsDel(CommonConsts.DEL);
            sysUserRoleMapper.update(sysUserRole, lambdaQueryWrapper);
            return Result.successMsg(ResultMsgEnum.SUCCESS_DELETE.getMsg());
        }
        return Result.fail(ResultMsgEnum.ERROR_OPERATE.getMsg());
    }
}
