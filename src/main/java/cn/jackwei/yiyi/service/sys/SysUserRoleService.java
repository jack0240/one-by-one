/*
 * Copyright (c) Jack魏 2023 - 2023, All Rights Reserved.
 */

package cn.jackwei.yiyi.service.sys;

import cn.jackwei.yiyi.mapper.sys.SysUserRoleMapper;
import cn.jackwei.yiyi.pojo.bean.SysUserRole;
import cn.jackwei.yiyi.pojo.enums.ResultMsgEnum;
import cn.jackwei.yiyi.util.Result;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 用户角色表 服务实现类
 * </p>
 *
 * @author Jack魏
 * @since 2023-07-21
 */
@Service
public class SysUserRoleService extends ServiceImpl<SysUserRoleMapper, SysUserRole> {
    /**
     * 给用户分配角色（一个用户一个角色）
     *
     * @param sysUserRole 用户角色信息
     * @return 新增、修改用户角色信息
     */
    public Result<String> editUserRole(SysUserRole sysUserRole) {
        LambdaQueryWrapper<SysUserRole> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(SysUserRole::getUserId, sysUserRole.getUserId());
        final List<SysUserRole> sysUserRoleList = baseMapper.selectList(wrapper);
        final boolean alive = sysUserRoleList != null && !sysUserRoleList.isEmpty();

        if (sysUserRole.getId() == null || sysUserRole.getId() == 0) {
            if (!alive) {
                final int flag = baseMapper.insert(sysUserRole);
                if (flag > 0) {
                    return Result.successMsg(ResultMsgEnum.SUCCESS_SAVE.getMsg());
                }
            } else if (!sysUserRoleList.get(0).getRoleId().equals(sysUserRole.getRoleId())) {
                sysUserRole.setId(sysUserRoleList.get(0).getId());
                final int flag = baseMapper.updateById(sysUserRole);
                if (flag > 0) {
                    return Result.successMsg(ResultMsgEnum.SUCCESS_UPDATE.getMsg());
                }
            } else {
                return Result.fail(ResultMsgEnum.ERROR_EXISTS.getMsg());
            }
        } else {
            final int flag = baseMapper.updateById(sysUserRole);
            if (flag > 0) {
                return Result.successMsg(ResultMsgEnum.SUCCESS_UPDATE.getMsg());
            }
        }
        return Result.fail(ResultMsgEnum.ERROR_OPERATE.getMsg());
    }

    /**
     * 批量取消用户角色
     *
     * @param userIds 用户id，用逗号隔开。
     * @return 是否成功
     */
    public Result<String> delUserRoleBatch(String userIds) {
        int num = baseMapper.delUserRoleBatch(userIds);
        if (num > 0) {
            return Result.successMsg("取消成功");
        }
        return Result.fail(ResultMsgEnum.ERROR_OPERATE.getMsg());
    }
}
