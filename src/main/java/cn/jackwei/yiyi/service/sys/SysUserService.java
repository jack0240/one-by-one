/*
 * Copyright (c) Jack魏 2022 - 2022, All Rights Reserved.
 */

package cn.jackwei.yiyi.service.sys;

import cn.jackwei.yiyi.mapper.sys.SysUserMapper;
import cn.jackwei.yiyi.pojo.bean.SysUser;
import cn.jackwei.yiyi.pojo.enums.ResultMsgEnum;
import cn.jackwei.yiyi.pojo.query.SysUserQuery;
import cn.jackwei.yiyi.util.Result;

import cn.hutool.core.text.CharSequenceUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 系统用户服务类
 *
 * @author Jack魏
 * @since 2022-11-12
 */
@Log4j2
@Service
public class SysUserService extends ServiceImpl<SysUserMapper, SysUser> {
    @Autowired
    private SysPageService sysPageService;

    @Autowired
    private SysUserMapper sysUserMapper;

    /**
     * 注册用户
     *
     * @param sysUser 用户信息
     * @return 注册用户
     */
    public SysUser register(SysUser sysUser) {
        int insertNum = saveUser(sysUser);
        if (insertNum > 0) {
            log.info("{}用户注册成功", sysUser.getAccount());
        } else {
            log.error("插入用户信息错误！");
            return null;
        }
        return sysUser;
    }

    /**
     * 保存用户，默认插入id
     *
     * @param sysUser 用户信息
     * @return 是否成功
     */
    private int saveUser(SysUser sysUser) {
        // 如果已经有用户名存在，注册就失败
        SysUser user = sysUserMapper.selectByAccount(sysUser.getAccount());
        if (user != null) {
            return 0;
        }
        sysUser.setId(IdUtil.fastSimpleUUID());
        return sysUserMapper.insert(sysUser);
    }

    /**
     * 根据登录名，获取用户信息
     *
     * @param account 登录名
     * @return 用户信息
     */
    public SysUser getByAccount(String account) {
        return sysUserMapper.selectByAccount(account);
    }

    /**
     * 根据用户id获取用户数据
     *
     * @param userId 用户id
     * @return 用户信息
     */
    public SysUser getById(String userId) {
        return sysUserMapper.selectById(userId);
    }

    /**
     * 新增、修改用户
     *
     * @param sysUser 用户信息
     * @return 新增、修改用户
     */
    public Result<String> editUser(SysUser sysUser) {
        // 新增
        if (CharSequenceUtil.isEmpty(sysUser.getId())) {
            final int insert = saveUser(sysUser);
            if (insert > 0) {
                return Result.successMsg(ResultMsgEnum.SUCCESS_SAVE.getMsg());
            } else {
                return Result.fail("账号已存在！");
            }
        }

        final int update = sysUserMapper.updateById(sysUser);
        if (update > 0) {
            return Result.successMsg(ResultMsgEnum.SUCCESS_UPDATE.getMsg());
        }

        return Result.fail(ResultMsgEnum.ERROR_OPERATE.getMsg());
    }

    /**
     * 获取用户信息，并查询所有权限页面
     *
     * @param userId 用户id
     * @return 用户信息
     */
    public SysUser getByIdAndPage(String userId) {
        SysUser sysUser = baseMapper.selectById(userId);
        if (sysUser != null) {
            sysUser.setPageList(sysPageService.listUserPageTree(userId));
        }
        return sysUser;
    }

    /**
     * 用户列表（分页）
     *
     * @param query 查询条件
     * @return 分页后用户信息和角色id
     */
    public Page<SysUser> pageUser(SysUserQuery query) {
        return baseMapper.pageUserAndRoleId(query);
    }
}
