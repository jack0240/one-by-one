/*
 * Copyright (c) Jack魏 2022 - 2022, All Rights Reserved.
 */

package cn.jackwei.yiyi.util;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 统一前端返回包装类
 *
 * @author Jack魏
 * @since 2022/12/30 20:53
 */
@Data
@Schema(description = "前端返回包装类")
public class Result<T> {
    /**
     * 成功返回编码
     */
    public static final Integer SUCCESS_CODE = 200;

    /**
     * 失败返回编码
     */
    public static final Integer ERROR_CODE = 500;

    /**
     * 成功信息
     */
    public static final String MESSAGE_OK = "ok";

    /**
     * 状态编码
     */
    @Schema(description = "状态编码")
    private Integer code;

    /**
     * 信息
     */
    @Schema(description = "返回消息")
    private String message;

    /**
     * 数据
     */
    @Schema(description = "返回数据")
    private T data;

    /**
     * 构造器
     *
     * @param data 数据
     * @param code 编码
     */
    public Result(T data, Integer code) {
        this.data = data;
        this.code = code;
        this.message = MESSAGE_OK;
    }

    /**
     * 构造器
     *
     * @param data    数据
     * @param code    编码
     * @param message 信息
     */
    private Result(T data, Integer code, String message) {
        this.data = data;
        this.code = code;
        this.message = message;
    }

    /**
     * 成功返回数据
     *
     * @param data 数据
     * @param <T>  泛型
     * @return 返回成功数据
     */
    public static <T> Result<T> success(T data) {
        return new Result<>(data, SUCCESS_CODE);
    }

    /**
     * 返回成功信息
     *
     * @param message 成功信息
     * @param <T>     泛型
     * @return 返回成功数据
     */
    public static <T> Result<T> successMsg(String message) {
        return new Result<>(null, SUCCESS_CODE, message);
    }

    /**
     * 失败返回数据
     *
     * @param message 数据
     * @param <T>     泛型
     * @return 返回成功数据
     */
    public static <T> Result<T> fail(String message) {
        return new Result<>(null, ERROR_CODE, message);
    }

    /**
     * 失败返回数据
     *
     * @param code    编码
     * @param message 数据
     * @param <T>     泛型
     * @return 返回成功数据
     */
    public static <T> Result<T> fail(Integer code, String message) {
        return new Result<>(null, code, message);
    }
}
