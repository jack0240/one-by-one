/*
 * Copyright (c) Jack魏 2023 - 2023, All Rights Reserved.
 */

package cn.jackwei.yiyi.util;

import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.symmetric.AES;
import cn.hutool.crypto.symmetric.SymmetricAlgorithm;
import lombok.extern.log4j.Log4j2;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

/**
 * 加解密工具类
 *
 * @author Jack魏
 * @since 2023/2/18 0:02
 */
@Log4j2
public class SecurityUtil {
    /**
     * 根据秘钥给内容加密为Base64
     *
     * @param content 加密内容
     * @param key     秘钥
     * @return 加密后数据
     */
    public static String encryptBase64(String content, String key) {
        try {
            KeyGenerator keyGenerator = KeyGenerator.getInstance(SymmetricAlgorithm.AES.getValue());
            SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG");
            secureRandom.setSeed(key.getBytes(StandardCharsets.UTF_8));
            keyGenerator.init(128, secureRandom);
            SecretKey secretKey = keyGenerator.generateKey();
            AES aes = SecureUtil.aes(secretKey.getEncoded());
            return aes.encryptBase64(content);
        } catch (NoSuchAlgorithmException exception) {
            log.error("AES加密失败：", exception);
        }
        return "";
    }

    /**
     * 解密
     *
     * @param encrypt 加密后数据
     * @param key     秘钥
     * @return 解密数据
     */
    public static String decryptStr(String encrypt, String key) {
        try {
            KeyGenerator keyGenerator = KeyGenerator.getInstance(SymmetricAlgorithm.AES.getValue());
            SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG");
            secureRandom.setSeed(key.getBytes(StandardCharsets.UTF_8));
            keyGenerator.init(128, secureRandom);
            SecretKey secretKey = keyGenerator.generateKey();
            AES aes = SecureUtil.aes(secretKey.getEncoded());
            return aes.decryptStr(encrypt);
        } catch (NoSuchAlgorithmException exception) {
            log.error("AES解密失败：", exception);
        }
        return "";
    }

    public static void main(String[] args) throws NoSuchAlgorithmException {
        String content = "test中文---000..'';;[[--==Jack";
        String key = "ik>$#vS%[#HU{KgaTEYMcQzm2ciJJW.guw3Y6Wt<@icQ4b0Z'R]b0s^Pe(NTd^P}Ct/Q^g[s;" +
            "KX4KsrVqmu!d_E%UF=IHJvX[JackWei1996_&(x-TsJOvi*f0M$Ee";

        String encrypt = encryptBase64(content, key);
        System.out.println(encrypt);
        System.out.println(decryptStr(encrypt, key));
        KeyGenerator keyGenerator = KeyGenerator.getInstance(SymmetricAlgorithm.AES.getValue());
        SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG");
        secureRandom.setSeed(key.getBytes(StandardCharsets.UTF_8));
        keyGenerator.init(128, secureRandom);
        SecretKey secretKey = keyGenerator.generateKey();
        AES aes = SecureUtil.aes(secretKey.getEncoded());
        encrypt = aes.encryptBase64(content);
        System.out.println(encrypt);

        String decryptStr = aes.decryptStr(encrypt);
        System.out.println(decryptStr);
    }
}
