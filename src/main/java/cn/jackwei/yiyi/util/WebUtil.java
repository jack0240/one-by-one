/*
 * Copyright (c) Jack魏 2022 - 2022, All Rights Reserved.
 */

package cn.jackwei.yiyi.util;

import cn.hutool.core.text.CharSequenceUtil;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.RandomUtil;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.log4j.Log4j2;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

/**
 * Web工具类
 *
 * @author Jack魏
 * @since 2022/11/16 21:02
 */
@Log4j2
public class WebUtil {
    /**
     * unknown常量
     */
    private static final String UNKNOWN = "unknown";
    /**
     * 随机密码取值
     */
    private static final String KEY = "abcdefghijklmnopqrstuvwxyz0123456789~!@#$%^&*()_+-/=[]{}:;'<>?" +
        ".ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    /**
     * 扰动次数
     */
    private static final int ENTROPY_NUM = 1000;

    /**
     * 不能实例化
     */
    private WebUtil() {
    }

    /**
     * 获取ip地址
     *
     * @param request 请求对象
     * @return ip
     */
    public static String getIpAddress(HttpServletRequest request) {
        String ip = request.getHeader("x-forwarded-for");
        ip = getHeaderValue(ip, "Proxy-Client-IP", request);
        ip = getHeaderValue(ip, "WL-Proxy-Client-IP", request);
        ip = getHeaderValue(ip, "HTTP_X_FORWARDED_FOR", request);
        ip = getHeaderValue(ip, "HTTP_X_FORWARDED", request);
        ip = getHeaderValue(ip, "HTTP_X_CLUSTER_CLIENT_IP", request);
        ip = getHeaderValue(ip, "HTTP_CLIENT_IP", request);
        ip = getHeaderValue(ip, "HTTP_FORWARDED_FOR", request);
        ip = getHeaderValue(ip, "HTTP_FORWARDED", request);
        ip = getHeaderValue(ip, "HTTP_VIA", request);
        ip = getHeaderValue(ip, "REMOTE_ADDR", request);
        if (CharSequenceUtil.isBlank(ip) || UNKNOWN.equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        // 本地地址
        if ("127.0.0.1".equals(ip) || "0:0:0:0:0:0:0:1".equals(ip)) {
            // 客户端和服务端是在同一台机器上、获取本机的IP
            try {
                InetAddress inetAddress = InetAddress.getLocalHost();
                ip = inetAddress.getHostAddress();
            } catch (UnknownHostException e) {
                log.error("ip error", e);
            }
        }
        return ip;
    }

    /**
     * 获取header值
     *
     * @param ip      ip地址
     * @param header  获取请求头key
     * @param request 请求
     * @return ip地址
     */
    private static String getHeaderValue(String ip, String header, HttpServletRequest request) {
        String newIp = ip;
        if (CharSequenceUtil.isBlank(ip) || UNKNOWN.equalsIgnoreCase(ip)) {
            newIp = request.getHeader(header);
        }
        return newIp;
    }

    /**
     * 根据请求获取参数
     *
     * @param request 请求
     * @return 请求参数map
     */
    public static Map<String, String> getParameterMap(HttpServletRequest request) {
        Map<String, String> requestParameterMap = new HashMap<>();
        Map<String, String[]> parameterMap = request.getParameterMap();
        if (parameterMap != null && parameterMap.size() > 0) {
            String key = null;
            String[] values = null;
            for (Map.Entry<String, String[]> entry : parameterMap.entrySet()) {
                key = entry.getKey();
                values = parameterMap.get(key);
                String value = ArrayUtil.isNotEmpty(values) ? ArrayUtil.join(values, ",") : "";
                requestParameterMap.put(key, value);
            }
        }
        return requestParameterMap;
    }

    /**
     * 随机指定长度密码
     *
     * @param size 长度
     * @return 密码
     */
    public static String getPassword(int size) {
        // 需要预热一定次数
        for (int i = 0; i < ENTROPY_NUM; i++) {
            RandomUtil.randomString(KEY, size);
        }
        return RandomUtil.randomString(KEY, size);
    }
}
