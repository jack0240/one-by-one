/*
 * Copyright (c) Jack魏 2023 - 2023, All Rights Reserved.
 */

package cn.jackwei.yiyi.util.code;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import java.util.Collections;

/**
 * MyBatis Plus代码生成
 * 官网文档：https://baomidou.com/pages/981406/#%E6%95%B0%E6%8D%AE%E5%BA%93%E9%85%8D%E7%BD%AE-datasourceconfig
 *
 * @author Jack魏
 * @since 2023/1/13 22:46
 */
public class AutoCode {
    /**
     * 数据库连接url
     */
    public static final String URL =
        "jdbc:mysql://127.0.0.1:3306/yiyi?serverTimezone=GMT%2B8&characterEncoding=utf-8&useSSL=true";
    /**
     * 数据库 用户名
     */
    public static final String USER_NAME = "root";
    /**
     * 数据库 密码
     */
    public static final String PASSWORD = "123456";
    /**
     * 作者
     */
    public static final String AUTHOR = "Jack魏";
    /**
     * java代码保存目录
     */
    public static final String SAVE_PATH = "E:\\Code\\git\\YiYi\\src\\main\\java";
    /**
     * xml保存目录
     */
    public static final String XML_SAVE_PATH = "E:\\Code\\git\\YiYi\\src\\main\\resources\\mapper";
    /**
     * 保存到包下面
     */
    public static final String PACKAGE = "cn.jackwei.yiyi";
    /**
     * 要生成的表
     */
    public static final String[] TABLES = {"sys_page", "sys_role"};

    /**
     * 代码生成入口
     *
     * @param args 参数
     */
    public static void main(String[] args) {
        FastAutoGenerator.create(URL, USER_NAME, PASSWORD).globalConfig(builder -> {
                builder.author(AUTHOR) // 设置作者
                    .outputDir(SAVE_PATH); // 指定输出目录
            }).packageConfig(builder -> {
                builder.parent(PACKAGE) // 设置父包名
                    .moduleName("sys") // 设置父包模块名
                    // 设置mapperXml生成路径
                    .pathInfo(Collections.singletonMap(OutputFile.xml, XML_SAVE_PATH));
            }).strategyConfig(builder -> {
                // 要生成的表
                builder.addInclude(TABLES);
            })
            // 使用Freemarker引擎模板，默认的是Velocity引擎模板
            .templateEngine(new FreemarkerTemplateEngine()).execute();
    }
}
