/*
 * Copyright (c) Jack魏 2022 - 2022 , All Rights Reserved.
 */

package cn.jackwei.yiyi;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * 测试类
 *
 * @author Jack魏
 * @since 2022-07-27
 */
@SpringBootTest
class YiYiApplicationTests {
    /**
     * 测试方法
     */
    @Test
    void contextLoads() {
        ConfigurableApplicationContext jack = SpringApplication.run(YiYiApplication.class, "jack");
        Assertions.assertNotNull(jack);
    }
}
