/*
 * Copyright (c) Jack魏 2022 - 2022 , All Rights Reserved.
 */

package cn.jackwei.yiyi.aop;

import cn.jackwei.yiyi.YiYiApplication;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 日志拦截类测试类
 *
 * @author Jack魏
 * @since 2022-11-05
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {YiYiApplication.class})
public class LogAspectTests {
    @Autowired
    private LogAspect logAspect;

    /**
     * logPointCut 测试
     */
    @Test
    public void logPointCutTest() {
        logAspect.logPointCut();
        Assertions.assertNull(null, "还不知道如何测试这里");
    }

    /**
     * logPointCut 测试
     */
    @Test
    public void doBefore() {
        Assertions.assertNull(null, "还不知道如何测试这里");
    }

    /**
     * logPointCut 测试
     */
    @Test
    public void logParams() {
        Assertions.assertNull(null, "还不知道如何测试这里");
    }
}
