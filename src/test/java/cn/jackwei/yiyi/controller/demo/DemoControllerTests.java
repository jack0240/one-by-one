/*
 * Copyright (c) Jack魏 2022 - 2022 , All Rights Reserved.
 */

package cn.jackwei.yiyi.controller.demo;

import cn.jackwei.yiyi.YiYiApplication;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 测试DemoController
 *
 * @author Jack魏
 * @since 2022-10-26
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {YiYiApplication.class})
public class DemoControllerTests {
    @Autowired
    private DemoController demoController;

    /**
     * 测试hello接口
     */
    @Test
    public void testHello() {
        String result = demoController.hello();
        Assert.assertEquals("返回错误！", "hello", result);
    }

    /**
     * 测试hello接口
     */
    @Test
    public void testHelloName() {
        String result = demoController.helloName("jack");
        Assert.assertEquals("返回错误！", "hello jack", result);
    }
}
