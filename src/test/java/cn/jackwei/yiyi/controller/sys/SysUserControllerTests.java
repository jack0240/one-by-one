/*
 * Copyright (c) Jack魏 2022 - 2022, All Rights Reserved.
 */

package cn.jackwei.yiyi.controller.sys;

import cn.jackwei.yiyi.YiYiApplication;
import cn.jackwei.yiyi.pojo.bean.SysUser;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 系统用户控制类测试类
 *
 * @author Jack魏
 * @since 2022-11-12
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {YiYiApplication.class})
public class SysUserControllerTests {
    @Autowired
    private SysUserController sysUserController;

    /**
     * 测试register接口
     */
    @Test
    public void register() {
        SysUser sysUser = new SysUser();
        String result = sysUserController.register(sysUser).getMessage();
        // 测试登录名为空
        Assertions.assertEquals("注册用户名不能为空", result);

        // 测试密码为空
        sysUser.setAccount("Jack魏");
        result = sysUserController.register(sysUser).getMessage();
        Assertions.assertEquals("密码不能为空", result);

        // 测试成功
        sysUser.setPassword("123456");
        result = (String) sysUserController.register(sysUser).getData();
        Assertions.assertEquals("注册成功，欢迎您：" + sysUser.getAccount(), result);
    }
}
