/*
 * Copyright (c) Jack魏 2023 - 2023, All Rights Reserved.
 */

package cn.jackwei.yiyi.mapper;

import cn.jackwei.yiyi.YiYiApplication;
import cn.jackwei.yiyi.pojo.bean.SysRole;
import cn.jackwei.yiyi.mapper.sys.SysRoleMapper;

import jakarta.annotation.Resource;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 用户角色Mapper测试
 * 这里MapperScan扫描的路径要改一下哟不然会报错:No qualifying bean of type 'cn.xxx.yiyi.sys.mapper
 * .SysPageMapper' available: expected at least 1 bean which qualifies as autowire candidate. Dependency annotations:
 * {@org.springframework.beans.factory.annotation.Autowired(required=true)}
 *
 * @author Jack魏
 * @since 2023/1/14 0:18
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {YiYiApplication.class})
@MapperScan("cn.jackwei.yiyi.sys.mapper")
public class SysRoleMapperTests {
    @Resource
    private SysRoleMapper sysRoleMapper;

    /**
     * 测试插入用户信息
     */
    @Test
    public void insertTest() {
        SysRole sysRole = new SysRole();
        sysRole.setName("测试");
        sysRole.setSort(1);
        int insertNum = sysRoleMapper.insert(sysRole);
        Assertions.assertEquals(1, insertNum);
    }
}
