/*
 * Copyright (c) Jack魏 2022 - 2022, All Rights Reserved.
 */

package cn.jackwei.yiyi.mapper;

import cn.jackwei.yiyi.YiYiApplication;
import cn.jackwei.yiyi.mapper.sys.SysUserMapper;
import cn.jackwei.yiyi.pojo.bean.SysUser;
import cn.jackwei.yiyi.pojo.query.SysUserQuery;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 系统用户Mapper测试类
 *
 * @author Jack魏
 * @since 2022-11-11
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {YiYiApplication.class})
public class SysUserMapperTests {
    @Autowired
    private SysUserMapper sysUserMapper;

    /**
     * 测试插入用户信息
     */
    @Test
    public void insertTest() {
        SysUser sysUser = new SysUser();
        sysUser.setUserName("Jack魏");
        sysUser.setAccount("Jack");
        sysUser.setPassword("123456");

        int insertNum = sysUserMapper.insert(sysUser);
        Assertions.assertEquals(1, insertNum);
    }

    @Test
    public void pageTest() {
        LambdaQueryWrapper<SysUser> lambdaQueryWrapper = new LambdaQueryWrapper<>();

        Page<SysUser> page = new Page<>(2, 1);
        final Page<SysUser> userPage = sysUserMapper.selectPage(page, lambdaQueryWrapper);
        System.out.println(userPage.getTotal());
    }

    @Test
    public void pageTest2() {
        LambdaQueryWrapper<SysUser> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        SysUserQuery page = new SysUserQuery();
        page.setCurrent(1);
        page.setSize(2);
        final Page<SysUser> userPage = sysUserMapper.selectPage(page, lambdaQueryWrapper);
        System.out.println(userPage.getTotal());
    }
}
