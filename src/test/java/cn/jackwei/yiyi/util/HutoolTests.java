/*
 * Copyright (c) Jack魏 2022 - 2023, All Rights Reserved.
 */

package cn.jackwei.yiyi.util;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.thread.ThreadUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.jwt.JWT;
import cn.hutool.jwt.JWTPayload;
import cn.hutool.jwt.JWTUtil;
import cn.hutool.jwt.signers.JWTSigner;
import cn.hutool.jwt.signers.JWTSignerUtil;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

/**
 * Hutool工具类测试
 *
 * @author Jack魏
 * @since 2022/11/29 22:35
 */
public class HutoolTests {
    /**
     * 测试Id生成
     */
    @Test
    public void testId() {
        String uuid = IdUtil.fastSimpleUUID();
        Assertions.assertEquals(32, uuid.length());
    }

    /**
     * 测试JWT签发
     */
    @Test
    public void testJWT() {
        Map<String, Object> payload = new HashMap<>();
        final DateTime now = DateTime.now();
        // 签发时间
        payload.put(JWTPayload.ISSUED_AT, now);
        // 过期时间，3秒后过期
        payload.put(JWTPayload.EXPIRES_AT, DateUtil.offsetSecond(now, 3));
        // 生效时间
        payload.put(JWTPayload.NOT_BEFORE, now);
        payload.put("userName", "Jack魏");
        payload.put("userId", "888888");

        String key = "Jack-Wei-Key";
        // 签名秘钥
        JWTSigner jwtSigner = JWTSignerUtil.hs512(key.getBytes(StandardCharsets.UTF_8));
        String jwtToken = JWTUtil.createToken(payload, jwtSigner);
        System.out.println(jwtToken);

        final boolean verify = JWTUtil.verify(jwtToken, jwtSigner);
        System.out.println(verify);

        // 验证过期
        ThreadUtil.safeSleep(4000);
        System.out.println(verify);
        // 解析token
        JWT jwt = JWTUtil.parseToken(jwtToken);
        // 验证秘钥和过期时间
        boolean verifyTime = jwt.setKey(key.getBytes(StandardCharsets.UTF_8)).validate(0);
        System.out.println(verifyTime);
        String userName = (String) jwt.getPayload("userName");
        String userId = (String) jwt.getPayload("userId");
        System.out.println("userName:" + userName + ", userId:" + userId);
    }
}
