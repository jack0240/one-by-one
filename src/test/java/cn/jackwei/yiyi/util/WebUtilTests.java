/*
 * Copyright (c) Jack魏 2022 - 2022, All Rights Reserved.
 */

package cn.jackwei.yiyi.util;

import lombok.extern.log4j.Log4j2;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

/**
 * Web工具类测试类
 *
 * @author Jack魏
 * @since 2022/11/16 21:02
 */
@Log4j2
public class WebUtilTests {
    /**
     * 测试ip地址
     */
    @Test
    public void getIpAddress() {
        Assertions.assertNull(null, "还不知道如何测试这里");
    }

    /**
     * 测试从请求头里面获取value
     */
    @Test
    public void getHeaderValue() {
        Assertions.assertNull(null, "还不知道如何测试这里");
    }

    /**
     * 测试参数转换成map
     */
    @Test
    public void getParameterMap() {
        Assertions.assertNull(null, "还不知道如何测试这里");
    }
}
